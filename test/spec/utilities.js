/* globals assert */
/* globals regexFromStringList */
/* globals generateValidateFunction */
(function () {
  'use strict';
  describe('Utilities.js', function () {
    describe('Pattern Matching', function () {
      describe('regexFromStringList',function () {
        it('should return a regexp', function () {
          var regex = regexFromStringList(['a','b']);
          assert.equal(regex instanceof RegExp, true);
        });
        it('should return a correct matching regex', function () {
          var regex = regexFromStringList(['a','b']);
          assert.equal(regex.toString(), '/a|b/i');
        });
      });
      describe('generateValidateFunction',function () {
        it('should validate string against a regex', function () {
          var validate = generateValidateFunction(/a|b/i);
          assert.equal(validate('a'),true);
          assert.equal(validate('c'),false);
        });
      });
    });
  });
})();
