Tabtruc
=======

# Tab archive
after `x` time :
* screenshot + archive
* put it in the archive group
* on display : only show the screenshot

on click on screenshot : 
* reload the url

# Tab Group

on new window : 
* new group

can delete the window or keep only this one

i.e. : 

```
group 1 :
o--o--o--o--o--o
   |
   o--o

group 2 :
o--o--o
   |
   o--o--o
      |
      o--o
```

# Group by site type

i.e. 

| Fb | SO | YT |
|----|----|----|
| .. | .. | .. |
| .. | .. | .. |
| .. | .. | .. |
| .. | .. | .. |

# Events
## tabs
* new tab : new entry : with url, parent, time, window, `focus`
* close tab : change entry : `close`
* change tab : change new : `focus` / old : `active`

## windows
* on window open : new window entry : time, `focus`
* on window close : change entry : `close`
* on window change : change window entry new : `focus` / old : `active`

# States
# possible states
| tab | window |
|-----|--------|
|active | active|
|focus | focus |
| archive | archive |
| close | close |

# transitions
|| active | archive | focus | close |
|-|--------|---------|-------|-------|
|active | | wait | focus | close |
| archive | | | unarchive | close |
| focus | unfocus | | | |
| close | | | ctrl + shift + t | |

# Datas
## Windows
* id (random/chrome.window id)
* creation date 
* tabs number
* state : active/passive/close
* current tab :
* tabs : []

## Tabs
* id (random/chrome.tab id)
* creation date 
* url
* state
* parent
* pinned
* blocked (should archive ?)
* next archive time 

# Benchmarks :

1. [{id:''}] or `{ id : {} }`
2. [{state : }] or [{ id }] + state : [ id ]
3. [{id, parent }] or [{id}] and children [id]
4. [copy of the useful datas] or [ copy of the chrome object ]
5. [{id, nexArchiveTime}] or nextArchiveTime : [{id:1}]
