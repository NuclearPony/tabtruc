'use strict';
var randomUrl = (function(){
	var domaines = ['binay','poulay'];//,'nobe4','SO','google', 'slack','averylongdomainename'];
	var paths = ['path','random'];//, 'cat', 'russian','red','blue'];
	return function(){
		return domaines[Math.floor(Math.random()*domaines.length)] +
			'/' + paths[Math.floor(Math.random()*paths.length)];
	};
})();
$(function(){
	$('li').click(function(e){
		$(e.target).parent('li').toggleClass('open');
		e.stopPropagation();
	});

	(function($, undefined){
		var TA = {
			links : {'root':[]},
			addButton : $(':button'),
			listWrapper : $('#list'),
			treeWrapper : $('#tree'),
			findParent : function(current,url){
				console.log(current, url);
			},
			add : function(url){
				var parent = TA.getRandomParent();
				var child = {};
				child[url] = [];
				TA.links[parent].push(child);
				console.log(TA.links);
			},
			getRandomParent : function(){
				return TA.links[Math.floor(Math.random()*TA.links.length)] || 'root';
			},
			setHandler : function(){
				TA.addButton.click(function(){
					TA.add(randomUrl());
				});
			},
			init : function(){
				TA.setHandler();
			}
		};
		TA.init();
	})($);
});

