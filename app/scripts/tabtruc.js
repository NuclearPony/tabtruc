/* tabtruc.js
 * This script handle the core of the extension.
 */
'use strict';

/* States for tab and window
 * @type {string}
 * [
 *   'active'   , the curent active view
 *   'passive'  , an unactive view that is not archived
 *   'archived' , an archived view
 *   'close'    , an archived and closed view
 * ]
 */

/* exported Tab */
var Tab = function(newTab){
  // variable definitions
  this.id = newTab.id;
  this.state = 'active';

  // method definitions
  this.changeState = function(newState){
    this.state = newState;
  };
};

/* exported Window */
var Window = function(newWindow){
  // variable definitions
  this.id = newWindow.id;
  this.state = 'active';
  this.currentTab = null;
  this.tabs = [];

  // method definitions
  this.newTab = function(tabId){
    this.tabs.push(tabId);
    this.currentTab = tabId;
  };
  this.changeTab = function(tabId){
    this.currentTab = tabId;
  };
  this.changeState = function(newState){
    this.state = newState;
  };
};

/* exported TabTruc */
/* main tabtruc object that contains all the definitions
*/
var TabTruc = function(){
  // variable definitions
  // id of the current window
  var currentWindow = null;
  // list of windows
  var windowsList = {};
  // list of tabs
  var tabsList = {};

  // event handler
  // Window events
  /* Handle the event when a new window is openend
   * @param {object} newWindowData the data from the newly created window
   * @param {object} newTabData the data from the newly created tab
   */
  this.onWindowOpen = function(newWindowData, newTabData){
    var newWindow = new Window(newWindow);
    var newTab = new Tab(newTab);
    newWindow.newTab(newTab.id);
  };
  this.onWindowClose = function (windowId, tabId) {
    delete windowsList[windowId];
    delete tabList[tabId];
  };

  this.onWindowChange = function(oldWindowId, newWindowId, oldTabId, newTabId) {
    windowsList[oldWindowId].state = 'passive';
    windowsList[newWindowId].state = 'active';
    tabsList[oldTabId].state = 'passive';
    tabsList[newTabId].state = 'active';
  };

  // Tab events
  this.onTabOpen = function(newTab, windowId){
    var newTab = new Tab(newTab);
    windowsList[windowId].newTab(newTab.id);
  };
  this.onTabChange = function(oldWindowId, newWindowId, oldTabId, newTabId) {
    windowsList[oldWindowId].state = 'passive';
    windowsList[newWindowId].state = 'active';
    tabsList[oldTabId].state = 'passive';
    tabsList[newTabId].state = 'active';
  };
  this.onTabClose = function(oldTab, windowId) {
    delete tabList[tabId];
    if ( tabList.lenght == 0 ) delete windowsList[windowId];
  };
};

// var saveTree = function() {
// console.log(state);
// chrome.storage.local.set({'state': state});
// };
// var generateUUID = function() {
// function s4() {return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);}
// return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
// };

// // function reset() {
// // state = {
// // currentGroup: null,
// // currentTab: null,
// // groupList: [],
// // tabList: []
// // };
// // saveTree();
// // }

// function getByFieldValue(array, field, value){
// return array.filter(function (entry) {
// return entry[field] === value;
// });
// }

// function closeTabsFromRootAndInsertOthers(callback){
// chrome.windows.get(rootWindow, {populate:true}, function(w){
// console.groupCollapsed('getTabToClose', w.id);
// console.log(w);
// var tabsToClose = [];
// for(var i in w.tabs){
// // if(!isURLWhitelisted(w.tabs[i].url))
// tabsToClose.push(w.tabs[i].id);
// // console.log(w.tabs[i].id);
// // console.log('B');
// // chrome.tabs.remove(w.tabs[i].id);
// }
// console.log(tabsToClose);

// callback();

// chrome.tabs.remove(tabsToClose);
// console.groupEnd();
// });
// }

// // var loadGroup = function(groupID) {
// // closeTabsFromRootAndInsertOthers(function(){
// // console.groupCollapsed('load Group ', groupID);
// // for(var i in state.tabList) {
// // console.log(state.tabList[i].groupID ,groupID);
// // if(state.tabList[i].groupID === groupID) {
// // // console.log('D');
// // (function(url){
// // console.log(url);
// // chrome.tabs.create({
// // windowId: rootWindow,
// // url: placeholder
// // }, function(tab){
// // chrome.tabs.sendMessage(tab.id,{url: url}, function(response) {
// // console.log(response.message);
// // });
// // });
// // })(state.tabList[i].url);
// // }
// // }
// // console.log(tabsToClose);
// // console.groupEnd();
// // });
// // };


// // var loadTree = function() {
// // chrome.storage.local.get(['state'], function(object){ state = object; });
// // };

// var removeUnusedWindow = function(){
// chrome.windows.getAll({},function(windows){
// console.groupCollapsed('removeUnusedWindow');
// console.log(windows);
// console.log(windows.length);
// if(windows.length > 1){
// for(var i in windows) {
// if(windows[i].id !== rootWindow){
// console.log('removing ',windows[i]);
// chrome.windows.remove(windows[i].id);
// }
// }
// }
// console.groupEnd();
// });
// };


// var newWindowHandler = function(window){
// console.groupCollapsed('newWindowHandler  ', window.id);
// if(rootWindow === null){
// console.log('init rootWindow', window.id);
// rootWindow = window.id;
// }

// removeUnusedWindow();

// var newId = generateUUID();
// state.groupList.push({
// id : newId,
// parentId : state.groupList.currentGroup,
// childrenId : [],
// title: 'Group ' + (new Date().getTime() / 1000).toFixed(0).substr(-5)
// });
// state.currentGroup = newId;
// saveTree();

// closeTabsFromRootAndInsertOthers(function(){
// chrome.tabs.create({'windowId': rootWindow, 'url': 'chrome://newtab'});
// });

// console.groupEnd();
// };
// chrome.windows.onCreated.addListener(newWindowHandler);

// var newTabHandler = function(tab){
// console.groupCollapsed('newTabHandler  ', tab.id);
// console.log('url : ', tab.url);
// if(!isURLWhitelisted(tab.url)) {
// var newId = generateUUID();
// state.tabList.push({
// id : newId,
// tabId : tab.id,
// title: tab.title,
// url : tab.url,
// groupID : state.currentGroup,
// screenshot: null,
// archived: false,
// important: false
// });
// var currentGroup = getByFieldValue(state.groupList, 'id', state.currentGroup)[0];
// console.log('currentGroup',currentGroup);
// if(currentGroup){
// currentGroup.childrenId.push(newId);
// state.currentTab = newId;
// }
// saveTree();
// }
// console.groupEnd();
// };
// chrome.tabs.onCreated.addListener(newTabHandler);


// var tabChangeHandler = function(id, changeInfo, tab){
// console.groupCollapsed('tabChangeHandler', tab.url);
// console.log(id, changeInfo, tab);
// if(!isURLWhitelisted(tab.url)) {
// console.log(id, changeInfo);
// if(changeInfo.status && changeInfo.status === 'loading'){
// console.log(changeInfo.url);
// var correspondingTab = getByFieldValue(state.tabList, 'tabId', id)[0];
// if(correspondingTab) {
// correspondingTab.url = changeInfo.url;
// }
// saveTree();
// }
// }
// console.groupEnd();
// };
// chrome.tabs.onUpdated.addListener(tabChangeHandler);



// // var spawnStuff = function() {
// // setTimeout(function(){
// // chrome.windows.create({}, function(window) {
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // });
// // },2000);
// // setTimeout(function(){
// // chrome.windows.create({}, function(window) {
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // });
// // },4000);
// // setTimeout(function(){
// // chrome.windows.create({}, function(window) {
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // chrome.tabs.create({'windowId': window.id, 'url': getRandomUrl()});
// // });
// // },6000);
// // }

// // var getRandomUrl = function() {
// // var a = [
// // 'http://google.com',
// // 'http://youtube.com',
// // 'http://gmail.com',
// // 'http://xkcd.com',
// // 'http://smbc-comics.com',
// // 'http://github.com',
// // ];

// // return a[Math.floor(a.length * Math.random())];
// // };

// // On new window : groupList.push(new), save state, loadGroup(groupList.last)
// // On new tab : tablList.push(new), save state
// // On tab update / close tab : update state, save state


// // chrome.tabs.onCreated.addListener(function callback(tab){
// // 	console.log(tab);
// // 	tab.bite = 'lol';

// // 	setTimeout(function(){
// // 		chrome.tabs.get(tab.id, function(t){
// // 			console.log(t)
// // 		});
// // 		// chrome.tabs.remove(tab.id);
// // 	},1000);
// // });
// // chrome.windows.onCreated.addListener(function callback(window){
// // 	chrome.windows.remove(window.id, function(){
// // 		chrome.windows.getCurrent({populate:true}, function(t){

// // 			console.log(t.tabs);
// // 			for(var i in t.tabs){
// // 				console.log(t.tabs[i]);
// // 				chrome.tabs.remove(t.tabs[i].id);
// // 			}
// // 		});
// // 	});
// // });
