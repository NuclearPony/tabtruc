/* utilities.js
 * this script will deal with all the non-chrome or non-tabtruc related work
 */
'use strict';

/* Pattern matching part */

/* exported regexFromStringList */
/* build a regexp from a list of string
 * @param {[string]} strings the array of strings to be used in the regex
 * @return {RegExp} the regex generated
 */
function regexFromStringList(strings){
  return new RegExp(strings.join('|'), 'i');
}

/* exported generateValidateFunction */
/* function that generate a validation function regex
 * @param {RegExp} regexp the regex to be used to test
 * @return {function} the validation function 
 */
function generateValidateFunction(regexp){
  /* function that will test the text against the regexp
   * @param {string} text the text to test
   * @return {boolean} whether the text match the regexp
   */
  return function(text){
    return regexp.test(text);
  };
}


/* Dummy content part */

/* exported getRandomUrl */
/* return a random url from a list 
 * @return {string} the random url
 */
function getRandomUrl() {
  var urls = [
    'http://google.com',
    'http://youtube.com',
    'http://gmail.com',
    'http://xkcd.com',
    'http://smbc-comics.com',
    'http://github.com'
  ];

  return urls[Math.floor(urls.length * Math.random())];
}

/* exported timeoutAndRepeat */
/* create a function that will call a certain number of time delayed a callback
 * the function will pass a random url each callback
 * @param {integer} timeout the number of second to delay
 * @param {integer} number the number of time to execute the callback
 * @param {function} callback the callback to be called
 */
function timeoutAndRepeat(timeout, number, callback){
  setTimeout(function() {
    for(var i = 0; i < number; i++){
      callback(getRandomUrl());
    }
  }, timeout || 2000); // default to 2 seconds
}

/* exported generateRandomUUID */
/* Create a random UUID used in the application
 * @return {string} the UUID generated
 */
function generateRandomUUID(){
  // generate a random hash 
  function s4() {return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);}
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
