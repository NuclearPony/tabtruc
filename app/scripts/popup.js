'use strict';
var background = chrome.extension.getBackgroundPage();

function onGroupClicked(groupID) {
  background.loadGroup(groupID);
}

function onGroupClickHandle(groupID) {
  return function(e){
    e.preventDefault();
    onGroupClicked(groupID);
  };
}

function listTabs(groupID) {
  var state = background.state;

  var tabList = document.createElement('ul');

  for(var i in state.tabList) {
    if(state.tabList[i].groupID === groupID) {
      var tab = state.tabList[i];

      var li = document.createElement('li');
      li.innerHTML = tab.url;
      tabList.appendChild(li);
    }
  }

  return tabList;
}

function listGroups() {
  var root = document.getElementById('root');
  root.innerHTML = '';

  var state = background.state;

  for(var i in state.groupList) {
    var group = state.groupList[i];

    // Create elements
    var li = document.createElement('li');

    var title = document.createElement('span');
    title.innerHTML = group.title;
    title.className = 'title';

    var tabList = listTabs(group.id);

    // Add click handler
    li.addEventListener('click', onGroupClickHandle(group.id));

    // Add everything to the page
    li.appendChild(title);
    li.appendChild(tabList);
    root.appendChild(li);
  }
}


listGroups();
